import React from "react";
import './App.css';
import TheNavbar from "./components/TheNavbar";
import Chat from "./components/Chat";
import  Main from "./components/Main";
import ScrollIndicator from "./scripts/ScrollIndicator";
import Footer from "./components/Footer";

function App() {
  return (
    <div>
        <TheNavbar />
        <ScrollIndicator />
        <Main />
        <Chat/>
        <Footer />

    </div>
  );
}

export default App;
