import React, {Component} from 'react';
import "../styles/Chat.css";
import ginaImage from "../images/ginaImage.png";
import axios from "axios";

class Chat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            text: "",
            chatHistory: []
        }
        this.buttonFunction = this.buttonFunction.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.jumpToTheEnd = this.jumpToTheEnd.bind(this);
    }

    getResponses(convertedString) {
        axios.get(`https://api.gina.within.space/messenger/call/${convertedString}`)
            .then((res) => {
                let newState = this.state.chatHistory;
                newState.push({content: res.data.response, chatClassName: "ChatMessageReceived"});
                this.setState({chatHistory: newState})
            })
            .catch(error => console.log(error))
    }

    buttonFunction() {
        let requestConvertedString = this.state.text.replace(/ /g, "%20");
        this.getResponses(requestConvertedString);
        let param = this.state.text;
        this.addUserText(param);
        this.resetChatInput();

    }

    resetChatInput() {
        this.setState({text: ""})
    }

    addUserText(textToInsert) {
        let newState = this.state.chatHistory;
        newState.push({content: textToInsert, chatClassName: "ChatMessageSent"});
        this.setState({chatHistory: newState})
    }

    handleChange(e) {
        this.setState({text: e.target.value})
    }

    jumpToTheEnd(){
    }
    render() {
        return (
            <div className="Chat col-12 col-md-6 mx-auto">
                <header className="ChatHeader">
                    <img className="ChatIcon" src={ginaImage} alt="Icon"/>
                    <h1 className="ChatVAName">Gina</h1>
                    <i className="ChatStatusIcon"> </i>
                    <h2 className="ChatStatus">Active now</h2>
                </header>
                <ul className="ChatConvo scroll">
                    {this.state.chatHistory.map(value =>
                        <li className={value.chatClassName}>{value.content}</li>
                    )}
                </ul>
                <footer className="container ChatFooter">
                    <div className="row">
                        <textarea className="col-9 ChatInput scroll" name="chatText"
                                  placeholder="Type..."
                                  value={this.state.text}
                                  onChange={this.handleChange}/>
                        <div className="col-3">
                            <button className="ChatSendButton" onClick={() => this.buttonFunction()}>Send</button>
                        </div>
                    </div>
                </footer>
            </div>
        );
    }
}

export default Chat;