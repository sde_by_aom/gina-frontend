import React, {Component} from 'react';
import {Container, Nav, Navbar, NavDropdown} from "react-bootstrap";
import 'bootstrap/dist/css/bootstrap.min.css';



class TheNavbar extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div>
                <Navbar bg="dark" variant="dark" expand="md">
                    <Container>
                        <Navbar.Brand href="#home">Virtual Assistant Gina</Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="me-auto">
                                <Nav.Link href="#home">Home</Nav.Link>
                                <Nav.Link href="#Gina">Gina</Nav.Link>
                                <NavDropdown title="Ce am folosit pentru acest proiect" id="basic-nav-dropdown">
                                    <NavDropdown.Item href="#react">React Js</NavDropdown.Item>
                                    <NavDropdown.Item href="#simfony">Symfony</NavDropdown.Item>
                                    <NavDropdown.Item href="#bootstrap">Bootstrap</NavDropdown.Item>
                                    <NavDropdown.Divider />
                                    <NavDropdown.Item href="#dialogflow">Dialogflow</NavDropdown.Item>
                                </NavDropdown>
                            </Nav>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>
            </div>
        );
    }
}

export default TheNavbar;