import React, {Component} from 'react';
import "../styles/Footer.css";

class Footer extends Component {
    render() {
        return (<div className="bg-dark text-light FooterContainer">
                <div className="container-fluid align-content-around">
                    <div className="row justify-content-between align-items-center text-md-center ps-5 ps-md-0 pt-2">
                        <div className="col-12 col-md-4">Virtual Assistant Gina</div>
                        <div className="col-12 col-md-4">Proiect realizat de:</div>
                        <div className="col-12 col-md-4">Misăilă Vlad & Negură Tiberiu</div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Footer;