import React, {Component} from 'react';
import {Container, Row} from "react-bootstrap";
import chat from "../images/chat.svg";
import softwareEngineer from "../images/undraw_software_engineer_re_fyew.svg";
import reactImage from "../images/React.svg";
import simfonyImage from "../images/symfony_black_03.svg";
import bootstrapImage from "../images/bootstrap-solid.svg";
import dialogflowImage from "../images/dialogflow.svg";

class Main extends Component {
    render() {
        return (
            <Container>
                <Row className="mt-5" id="home">
                    <img className="col-12 col-md-7 mt-3 mt-md-0" src={chat} alt=" "/>
                    <div className="col-12 col-md-5">
                        <h1 className="h1 my-5">Ce este un Chatbot?</h1>
                        <p className="lead">
                            La cel mai elementar nivel, un chatbot este un program de calculator care simulează și
                            procesează conversația umană (fie scrisă sau vorbită), permițând oamenilor să interacționeze
                            cu
                            dispozitivele digitale ca și cum ar comunica cu o persoană reală. Chatbot-urile pot fi la
                            fel de
                            simple ca programele rudimentare care răspund la o întrebare simplă cu un răspuns pe o
                            singură
                            linie, sau la fel de sofisticate ca asistenții digitali care învață și evoluează pentru a
                            oferi
                            niveluri tot mai mari de personalizare pe măsură ce adună și procesează informații.
                        </p>
                    </div>
                </Row>
                <Row className="mt-5">
                    <div className="col-12 col-md-5">
                        <h1 className="h1">De ce un Chatbot?</h1>
                        <p className="lead">
                            Primul beneficiu al chatbot-urilor este că pot lucra 24 de ore pe zi, 7 zile pe săptămână.
                            De asemenea, dacă vreodată un chabot se chinuie să înțeleagă cererile unor utilizatori și se
                            blochează încercând să răspundă la întrebările lor sau să rezolve probleme, poate delega
                            solicitări dificile către echipa ta de asistență umană.
                            <br/>
                            <br/>
                            În prezent, SuperOffice a aflat că 88% dintre clienți așteaptă un răspuns de la companii în
                            60 de minute și că 30% așteaptă un răspuns în 15 minute sau mai puțin.
                            <br/>
                            <br/>
                            Un studiu realizat de consiliul CMO a mai descoperit că abilitatea de a oferi răspunsuri
                            rapide clienților este cel mai important atribut pentru o experiență bună a clienților.
                        </p>
                    </div>
                    <img className="col-12 col-md-7 mt-3 mt-md-0" src={softwareEngineer} alt=" "/>
                </Row>
                <Row className="mt-5" id="react">
                    <div className="col-12 col-md-6">
                        <img className="col-12 mt-3 mt-md-0" src={reactImage} alt=" "/>
                        <h1 className="h1">React Js</h1>
                        <p className="lead">
                            React este o librărie JavaScript creată de Facebook. De asemenea, este și o unealtă pentru
                            construirea interfertelor pentru utilizatori. Este bazat pe componente, care își gestionează
                            propria stare, apoi se compun pentru a crea interfețe de utilizare complexe.<br/>Deoarece
                            logica
                            componentelor este scrisă în JavaScript în loc de șabloane, se pot trece cu ușurință pachete
                            mari de prin aplicație și poate păstra starea în afară DOM-ului.

                        </p>
                    </div>
                    <div className="col-12 col-md-6" id="simfony">
                        <img className="col-12 mt-3 mt-md-0" src={simfonyImage} alt=" "/>

                        <h1 className="h1">Simfony</h1>
                        <p className="lead">
                            Principalul framework de PHP pentru crearea de site-uri și aplicații web. Construit pe un
                            set de componente decuplate și reutilizabile pe care sunt construite cele mai bune aplicații
                            PHP, cum ar fi Drupal, phpBB și eZ Publish.

                            <br/>
                            <br/>
                            În prezent, SuperOffice a aflat că 88% dintre clienți așteaptă un răspuns de la companii în
                            60 de minute și că 30% așteaptă un răspuns în 15 minute sau mai puțin.
                            <br/>
                            <br/>
                            Un studiu realizat de consiliul CMO a mai descoperit că abilitatea de a oferi răspunsuri
                            rapide clienților este cel mai important atribut pentru o experiență bună a clienților.
                        </p>
                    </div>
                </Row>
                <Row className="mt-5" id="bootstrap">
                    <div className="col-12 col-md-6 mx-md-auto">
                        <img className="col-12 mt-3 mt-md-0" src={bootstrapImage} alt=" " height={{"height": "50%"}}/>
                        <h1 className="h1">Bootsrtap</h1>
                        <p className="lead">
                            Bootstrap este un frontend development framework gratuit și open source pentru crearea de
                            site-uri și aplicații web.<br/>
                            El este special proiectat și personalizat rapid pentru site-uri responsive, axat pe
                            dispozitivele mobile,
                            astfel Bootstrap devine cel mai popular set de instrumente open source de front-end din
                            lume, care
                            include variabile Sass, responsive grid system, componente extinse predefinite
                            și plugin-uri JavaScript bine compuse.
                        </p>
                    </div>
                </Row>
                <Row className="mt-5" id="dialogflow">
                    <img className="col-12 col-md-7 mt-5 pt-5 mt-md-0" src={dialogflowImage} alt=" "/>
                    <div className="col-12 col-md-5">
                        <h1 className="h1 my-5">Dialogflow</h1>
                        <p className="lead">
                            Dialogflow este o platformă de înțelegere a limbajului natural care facilitează proiectarea
                            și integrarea unei interfețe de utilizator conversaționale într-o aplicație mobilă,
                            aplicația web, dispozitivul, botul, sistemul interactiv de răspuns vocal și așa mai departe.

                        </p>
                    </div>
                    <div className="col-12">
                        <p className="lead">
                            Folosind Dialogflow, se pot oferi utilizatorilor moduri noi și captivante de a interacționa
                            cu produsul.
                            <br/>
                            Dialogflow poate analiza mai multe tipuri de intrări de la clienți, inclusiv intrări
                            text sau audio (cum ar fi de la un telefon sau o înregistrare vocală). De asemenea, poate
                            răspunde clienților în câteva moduri, fie prin text, fie prin utilizarea unui sintetizator
                            vocal.
                            <br/>
                            Dialogflow CX și ES oferă servicii de agenți virtuali pentru chatbot și centre de
                            contact.
                            Dacă este un centru de contact care angajează agenți umani, se poate utiliza Agent
                            Assist
                            pentru a ajuta agenții umani. Agent Assist oferă sugestii în timp real pentru agenții
                            umani în timp ce aceștia sunt în conversații cu clienții(end-users).
                            <br/>
                            API-ul Agent Assist este implementat ca o extensie a API-ului Dialogflow ES. Când
                            răsfoiți
                            API-ul Dialogflow ES, veți vedea aceste tipuri și metode suplimentare.
                        </p>!g
                    </div>
                </Row>
                <Row>
                    <div className="mx-auto" id="Gina">
                        <h1 className="h1 mt-5">Virtual Assistant Gina</h1>
                        <p className="lead">
                            Gina este un asistent virtual care simulează conversația umană, permițând utilizatorilor să
                            afle
                            informațiile dorite la un chat distanță. Gina procesează datele și își mărește knowledge
                            base-ul
                            și astfel își crește eficiența și randamentul.
                        </p>
                    </div>

                </Row>
            </Container>
        )
    }
}

export default Main;